import React from 'react';

const Footer = () => (
  <div className='footer'>
    <h1 href='/' className='textCenter'>
      MyStore
    </h1>
    <p className='textCenter'>2021 MyStore. All rights reserved. </p>
  </div>
);

export default Footer;
